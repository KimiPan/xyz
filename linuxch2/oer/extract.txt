Regression: Record Publish: GOPI .W
Regression:   Type: trade
Regression:   wMsgType | int | * | 13
Regression:   wPubId | string | * | opra_8
Regression:   wIssueSymbol | string | * | GOPI .W
Regression:   wEntitleCode | int | * | 77
Regression:   wDisplayHint | int | * | 73
Regression:   wInstrumentType | enum | * | Option
Regression:   wSessionId | char | * |  
Regression:   wPartRecord | bool |   | 1
Regression:   wConsRecord | bool |   | 0
Regression:   wQualBbo | bool |   | 1
Regression:   wBidPartId | string | * | W
Regression:   wBidPrice | price | * | 35.20
Regression:   wBidSize | quantity | * | 70
Regression:   wAskPrice | price | * | 36.40
Regression:   wAskSize | quantity | * | 42
Regression:   wQuoteQualifier | string | * | Normal
Regression:   wSecurityStatus | enum | * | Normal
Regression:   wBidHigh | price | * | 186.50
Regression:   wBidLow | price | * | 18.60
Regression:   wAskPartId | string | * | W
Regression:   wAskHigh | price | * | 190.30
Regression:   wAskLow | price | * | 20.20
Regression:   wQuoteDate | date | * | 2010/07/05
Regression:   wQuoteTime | time | * | 13:54:00.019
Regression:   wQuoteSeqNum | int | * | 13878701
Regression:   wStrikePrice | price | * | 570.00
Regression:   wCondition | char | * |  
Regression:   wLineTime | time | * | 04:36:47.107
Regression:   wSrcTime | time | * | 13:54:00.019
Regression:   wOpenPrice | price | * | 26.80
Regression:   wTradeDate | date | * | 2010/07/05
Regression:   wLowPrice | price | * | 26.80
Regression:   wHighPrice | price | * | 123.00
Regression:   wTradeCount | int | * | 3
Regression:   wTotalVolume | quantity | * | 12
Regression:   wTotalValue | double | * | 511.8
Regression:   wVwap | double | * | 42.65
Regression:   wNetChange | price | * | 96.20
Regression:   wPctChange | double | * | 358.96
Regression:   wTradeTick | enum | * | +
Regression:   wIsIrregular | bool | * | 0
Regression:   wTradePrice | price | * | 123.00
Regression:   wTradeTime | time | * | 13:54:20.469
Regression:   wTradeUnits | string | * | 2DP
Regression:   wTradeVolume | quantity | * | 1
Regression:   wTradePartId | string | * | W
Regression:   wTradeQualifier | string | * | Normal
Regression:   wTradeSeqNum | int | * | 14071132
Regression:   wSaleCondition | string | * |  
Regression: Record Publish: GOPI .W
Regression:   Type: Quote
Regression:   wMsgType | int | * | 13
Regression:   wPubId | string |   | opra_8
Regression:   wIssueSymbol | string | * | GOPI .W
Regression:   wEntitleCode | int | * | 77
Regression:   wDisplayHint | int |   | 73
Regression:   wInstrumentType | enum |   | Option
Regression:   wSessionId | char |   |  
Regression:   wPartRecord | bool |   | 1
Regression:   wConsRecord | bool |   | 0
Regression:   wQualBbo | bool |   | 1
Regression:   wBidPartId | string | * | W
Regression:   wBidPrice | price | * | 157.60
Regression:   wBidSize | quantity | * | 43
Regression:   wAskPrice | price | * | 160.90
Regression:   wAskSize | quantity | * | 31
Regression:   wQuoteQualifier | string | * | Normal
Regression:   wSecurityStatus | enum | * | Normal
Regression:   wBidHigh | price |   | 186.50
Regression:   wBidLow | price |   | 18.60
Regression:   wAskPartId | string | * | W
Regression:   wAskHigh | price |   | 190.30
Regression:   wAskLow | price |   | 20.20
Regression:   wQuoteDate | date |   | 2010/07/05
Regression:   wQuoteTime | time | * | 13:54:00.019
Regression:   wQuoteSeqNum | int | * | 13878702
Regression:   wStrikePrice | price | * | 410.00
Regression:   wCondition | char | * |  
Regression:   wLineTime | time | * | 04:36:47.107
Regression:   wSrcTime | time | * | 13:54:00.019
Regression:   wOpenPrice | price |   | 26.80
Regression:   wTradeDate | date |   | 2010/07/05
Regression:   wLowPrice | price |   | 26.80
Regression:   wHighPrice | price |   | 123.00
Regression:   wTradeCount | int |   | 3
Regression:   wTotalVolume | quantity |   | 12
Regression:   wTotalValue | double |   | 511.8
Regression:   wVwap | double |   | 42.65
Regression:   wNetChange | price |   | 96.20
Regression:   wPctChange | double |   | 358.96
Regression:   wTradeTick | enum |   | +
Regression:   wIsIrregular | bool |   | 0
Regression:   wTradePrice | price |   | 123.00
Regression:   wTradeTime | time |   | 13:54:20.469
Regression:   wTradeUnits | string |   | 2DP
Regression:   wTradeVolume | quantity |   | 1
Regression:   wTradePartId | string |   | W
Regression:   wTradeQualifier | string |   | Normal
Regression:   wTradeSeqNum | int |   | 14071132
Regression:   wSaleCondition | string |   |  
Regression: Record Publish: GOPU .W
Regression:   Type: Quote
Regression:   wMsgType | int | * | 13
Regression:   wPubId | string | * | opra_8
Regression:   wIssueSymbol | string | * | GOPU .W
Regression:   wEntitleCode | int | * | 77
Regression:   wDisplayHint | int | * | 73
Regression:   wInstrumentType | enum | * | Option
Regression:   wSessionId | char | * |  
Regression:   wPartRecord | bool |   | 1
Regression:   wConsRecord | bool |   | 0
Regression:   wQualBbo | bool |   | 1
Regression:   wBidPartId | string | * | W
Regression:   wBidPrice | price | * | 11.10
Regression:   wBidSize | quantity | * | 45
Regression:   wAskPrice | price | * | 11.50
Regression:   wAskSize | quantity | * | 63
Regression:   wQuoteQualifier | string | * | Normal
Regression:   wSecurityStatus | enum | * | Normal
Regression:   wBidHigh | price | * | 61.70
Regression:   wBidLow | price | * | 1.40
Regression:   wAskPartId | string | * | W
Regression:   wAskHigh | price | * | 63.80
Regression:   wAskLow | price | * | 1.60
