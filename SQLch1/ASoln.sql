SELECT  
SUM(`<vol>`*`<close>`)/SUM(`<vol>`) as VOLUME_WEIGHTED_PRICE ,
date_format(`<date>`,"%d/%M/%Y") as 'Date',
min(date_format(`<date>`,"%H:%i")) as 'Stard',
max(date_format(`<date>`,"%H:%i")) as 'End'
FROM sample_dataset
WHERE `<date>` BETWEEN '2010-10-11 09:00:00' AND '2010-10-11 14:00:00';
