SELECT TRADING_SYMBOL,
SUM(TRADE_SIZE*TRADE_PRICE)/SUM(TRADE_SIZE) as VOLUME_WEIGHTED_PRICE
FROM STOCK_TRADE
WHERE TRADE_TIME BETWEEN '2005-11-14 12:00' AND '2005-11-14 15:00'
AND TRADING_SYMBOL = 'ADV'
GROUP BY TRADING_SYMBOL